'use strict';

const Hapi = require('@hapi/hapi');
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');
const Vision = require('@hapi/vision');
const wreck = require('@hapi/wreck');
const Inert = require('@hapi/inert');
const Path = require('path');

// config variables
const config = require('./config/config.js');

const rootHandler = (request, h) => {

    return h.view('index', {
        title: 'Loja' + request.server.version,
        message: 'msg123'
    });
};

const init = async () => {

    const server = Hapi.server({
        port: global.gConfig.node_port,
        host: 'localhost',
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'src')
            }
        }
    });

    await server.register([
        Vision,
        Inert,
        require('./plugins/estoque'),
        require('./plugins/provider')        
    ]);

    server.views({
        engines: {
            pug: require('pug')
        },
        relativeTo: __dirname,
        path: 'templates'
    });

    server.route({
        method: 'GET',
        path: '/',
        handler: rootHandler
    });

    server.route({
        method: 'GET',
        path: '/{filename}',
        handler: {
            file: function (request) {
                return request.params.filename;
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: '.'
            }
        }
    });


    await server.start();
    console.log('Front running on %s', server.info.uri);
};

init();