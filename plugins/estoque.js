'use strict';

const Wreck = require('@hapi/wreck');
require ('../config/config');
const estoque_base_url = global.gConfig.stock_url + '/estoque';

const estoque = {
    name: 'stock',
    version: '1.0.0',
    register: async function (server, options) {
        server.route({
            method: 'GET',
            path: '/stock',
            handler: async function (request, h) {
                const promise = Wreck.request('GET', estoque_base_url);
                try{
                    const res = await promise;
                    return h.view('stock', {
                        title: 'Loja' + request.server.version,
                        list: res
                    });
                }catch{
                    return [];
                }
            }
        })
    }
};

module.exports = estoque