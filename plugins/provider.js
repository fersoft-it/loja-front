'use strict';

const Wreck = require('@hapi/wreck');
require('../config/config');
const estoque_base_url = global.gConfig.provider_url + '/provider';

const provider = {
    name: 'provider',
    version: '1.0.0',
    register: async function (server, options) {
        server.route({
            method: 'GET',
            path: '/provider',
            handler: async function (request, h) {
                const promise = await Wreck.request('GET', estoque_base_url);
                try {
                    const body = await Wreck.read(promise, {json: true});
                    return h.view('provider', {
                        title: 'Loja' + request.server.version,
                        list: body
                    });
                } catch {
                    return [];
                }
            }
        })

        server.route({
            method: 'POST',
            path: '/provider/edit',
            handler: async (request, h) => {
                let options = {
                    payload: request.payload
                }

                console.log('saving provider', options);

                const promise = await Wreck.request('POST', estoque_base_url, options);
                const body = await Wreck.read(promise, {
                    json: true
                });

                return h.redirect('/provider')
            }
        })

        server.route({
            method: 'GET',
            path: '/provider/edit',
            handler: {
                view: {
                    template: 'provider-edit',
                    context: {

                    }
                }
            }
        })
    }
};

module.exports = provider